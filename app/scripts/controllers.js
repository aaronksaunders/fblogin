'use strict';
angular.module('Fblogin.controllers', [])

    .controller('AppCtrl', function ($scope) {
    })

    .controller('PlaylistsCtrl', function ($scope) {
        $scope.playlists = [
            { title: 'Reggae', id: 1 },
            { title: 'Chill', id: 2 },
            { title: 'Dubstep', id: 3 },
            { title: 'Indie', id: 4 },
            { title: 'Rap', id: 5 },
            { title: 'Cowbell', id: 6 }
        ];
    })

    .controller('PlaylistCtrl', function ($scope, $stateParams) {
// https://github.com/andreassolberg/jso/blob/master/README-Phonegap.md

        debugger;
        var debug = true, inAppBrowserRef;
        var localStoreKey = "411eats";


        jso_wipe();

        function locationChange(url) {
            outputlog("in location change");
            url = decodeURIComponent(url);
            outputlog("Checking location: " + url);

            jso_checkfortoken('facebook', url, function () {
                outputlog("Closing InAppBrowser, because a valid response was detected.");
                setTimeout(function () {
                    inAppBrowserRef.close();
                }, 200);
            });
        }


        /*jso_registerRedirectHandler(function (url) {
         inAppBrowserRef = window.open(url, "_blank");
         inAppBrowserRef.addEventListener('loadstop', function (e) {
         locationChange(e.url);
         }, false);
         });
         */
        if (false) {
            /*
             * Configure the OAuth providers to use.
             */
            jso_configure({
                "facebook": {
                    client_id: "1394911154117635",
                    redirect_uri: "http://www.facebook.com/connect/login_success.html",
                    authorization: "https://www.facebook.com/dialog/oauth",
                    presenttoken: "qs"
                }
            }, {"debug": debug});

            // For debugging purposes you can wipe existing cached tokens...
            jso_wipe();

            // jso_dump displays a list of cached tokens using console.log if debugging is enabled.
            jso_dump();

            // Perform the protected OAuth calls.
            $.oajax({
                url: "https://graph.facebook.com/me/home",
                jso_provider: "facebook",
                jso_scopes: ["read_stream"],
                jso_allowia: true,
                dataType: 'json',
                success: function (data) {

                    console.log("HELLO");

                    var i, l, item;
                    outputlog("Response (facebook):");
                    //outputlog(data.data);
                    try {
                        for (i = 0, l = data.data.length; i < l; i++) {
                            item = data.data[i];
                            outputlog("\n");
                            outputlog(item.story || [item.from.name, ":\n", item.message].join(""));
                        }
                    }
                    catch (e) {
                        outputlog(e);
                    }
                },
                error: function (_e) {
                    console.log(_e);
                }
            });
        } else {
            var options = {
                consumerKey: 'PU47tNh833f96TZ1OlrspA',
                consumerSecret: '7kx4gVW1UBIEeSOgwGm6jdNNqq27gl5kYCfkWAC6XW4',
                callbackUrl: 'http://www.clearlyinnovative.com/oauth.html',
                requestTokenUrl: "https://api.twitter.com/oauth/request_token",
                authorizationUrl: "https://api.twitter.com/oauth/authenticate",
                accessTokenUrl: "https://api.twitter.com/oauth/access_token"
            };


            var storedAccessData, rawData = window.localStorage.getItem(localStoreKey);
            var requestParams, mentionsId, oauth = new OAuth(options);
            storedAccessData = rawData ? JSON.parse(rawData) : null;

            if (storedAccessData ) {

                console.log("storedAccessData " + JSON.stringify(storedAccessData, null, 2) + " ");

                oauth = OAuth(options);
                oauth.setAccessToken([storedAccessData.accessTokenKey, storedAccessData.accessTokenSecret]);
                console.log("options " + JSON.stringify(options, null, 2));

                oauth.fetchAccessToken(
                    function (data) {
                        console.log("verify_credentials " + JSON.stringify(data, null, 2));
                    }, function error(_error) {
                        console.log("verify_credentials ERROR " + JSON.stringify(_error, null, 2));
                    });
            } else {

                oauth.fetchRequestToken(function openAuthoriseWindow(_url) {
                    debugger;
                    inAppBrowserRef = window.open(_url, "_blank");
                    inAppBrowserRef.addEventListener('loadstop', function (e) {
                        var url = e.url;


                        if (url.indexOf("clearlyinnovative.com") === -1) {
                            return;
                        }

                        url = decodeURIComponent(url);
                        outputlog("Checking location: " + url);

                        var params = checkTwitterUrl(url);

                        //if (params['oauth_token_secret']) {
                        //    oauth._secret = params['oauth_token_secret'];
                        //
                        //}

                        if (false && params['oauth_verifier']) {
                            // Exchange request token for access token
                            oauth.get('https://api.twitter.com/oauth/access_token?oauth_verifier=' + params['oauth_verifier'] + '&' + requestParams,
                                function (data) {
                                    var accessParams = {};
                                    var qvars_tmp = data.text.split('&');
                                    for (var i = 0; i < qvars_tmp.length; i++) {
                                        var y = qvars_tmp[i].split('=');
                                        accessParams[y[0]] = decodeURIComponent(y[1]);
                                    }
                                    console.log('twitter data: ' + accessParams.oauth_token + ' : ' + accessParams.oauth_token_secret);
                                    oauth.setAccessToken([accessParams.oauth_token, accessParams.oauth_token_secret]);

                                    // Save access token/key in localStorage
                                    saveLocalAccess(oauth.getAccessToken());

                                    inAppBrowserRef.close();
                                },
                                function (data) {
                                    alert('Error : No Authorization');
                                    console.log("Error " + data);

                                }
                            );

                        }
                        //if (params['oauth_verifier']) {
                        outputlog("Closing InAppBrowser, because a valid response was detected.");
                        setTimeout(function () {
                            inAppBrowserRef.close();

                            saveLocalAccess(oauth.getAccessToken());

                            //oauth.setAccessToken([ params['?oauth_token'], oauth._secret ]);
                            oauth.setVerifier(params['oauth_verifier']);
                            oauth.fetchAccessToken(getSomeData, failureHandler);
                        }, 200);
                        //}
                    }, false);
                }, function failureHandler(data) {
                    console.error(data);
                });
            }
        }

        var parseQueryString = function (qs) {
            var e,
                a = /\+/g, // Regex for replacing addition symbol with a space
                r = /([^&;=]+)=?([^&;]*)/g,
                d = function (s) {
                    return decodeURIComponent(s.replace(a, " "));
                },
                q = qs,
                urlParams = {};

            while (e = r.exec(q))
                urlParams[d(e[1])] = d(e[2]);

            return urlParams;
        };

        function getSomeData() {
            oauth.get('https://api.twitter.com/1.1/statuses/home_timeline.json?count=2', gotData, failureHandler);
        }

        function gotData(_data) {
            console.log("gotData " + JSON.stringify(_data));
        }

        function checkTwitterUrl(_url) {
            var h = _url.substring(_url.indexOf('?'));
            var params = parseQueryString(h);
            console.log("checkTwitterUrl " + JSON.stringify(params));
            return params;
        }

        function failureHandler(data) {
            console.error(data);
        }

        function saveLocalAccess(accessParams) {
            console.log("AppLaudLog: Storing token key/secret in localStorage " + JSON.stringify(accessData));
            // Save access token/key in localStorage
            var accessData = {};
            accessData.accessTokenKey = accessParams[0];
            accessData.accessTokenSecret = accessParams[1];
            window.localStorage.setItem(localStoreKey, JSON.stringify(accessData));
        }

        function outputlog(m) {
            var t = typeof m === 'string' ? m : JSON.stringify(m);
            t += '\n';
            console.log(t);
        }


    });
